root-tail (1.3-3) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * Update watch file format version to 4.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 02 Nov 2022 21:07:52 +0000

root-tail (1.3-2) unstable; urgency=low

  * QA upload.
  * Replace -Wl,--no-as-needed with putting $(LDFLAGS) at the end. Thanks
    to Marc Lehmann (upstream) for that hint!
  * Drop config.h.patch. #298708 is no more reproducible and probably was
    a bug elsewhere. (Closes: #930581)

 -- Axel Beckert <abe@debian.org>  Sun, 01 Sep 2019 16:06:31 +0200

root-tail (1.3-1) unstable; urgency=low

  * QA upload.
  * Update Homepage header, debian/copyright and debian/watch for new
    project location.
  * Update Vcs-* headers for move to Salsa.
  * Add duck-override for ancient, probably on-purpose incomplete e-mail
    address in debian/copyright.
  * Import new upstream release 1.3. (Closes: #930582)
    + Implementes a -windowed mode which provides a transparent override
      redirect window. (Closes: #118666)
    + Add new build-dependencies on libxext-dev and libxfixes-dev.
    + Remove debian-provided Makefile, upstream now ships one.
    + Drop root-tail.c.patch and root-tail.man.patch, applied upstream.
    + Refresh remaining config.h.patch and add Bug-Debian header to it.
    + Patch Makefile to not hardcode and override $LDFLAGS and $CFLAGS.
    + Patch Makefile to add --Wl,--no-as-needed to avoid FTBFS with GCC 9.
    + Patch Makefile to also pass $CPPFLAGS (as in Debian's Makefile from
      previous package releases) to avoid hardening-no-fortify-functions
      lintian warning.
  * Remove trailing whitespace from ancient debian/changelog entries.
  * Bump debhelper compatibility level to 12.
    + Replace debian/compat with a versioned b-d on debhelper-compat.
  * Declare compliance with Debian Policy 4.4.0. (No changes needed.)
  * Set "Rules-Requires-Root: no".
    + Patch Makefile to not try to redundantly chown anything.

 -- Axel Beckert <abe@debian.org>  Sun, 01 Sep 2019 01:02:52 +0200

root-tail (1.2-4) unstable; urgency=medium

  * QA upload.
    + Set Maintainer to Debian QA Group. (See #838406)
    + Add Vcs-* headers for newly created collab-maint Git repo.
  * Apply wrap-and-sort.
  * Add Homepage header.
  * Bump debhelper compatibility level to 9. (Closes: #817656)
    + Update versioned build-dependency on debhelper accordingly.
  * Add dependency on ${misc:Depends}.
  * Rewrite debian/rules in minimal dh v7 style.
  * Enable all hardening build flags.
  * Convert to source format "3.0 (quilt)" using diff2patches.
    + Add DEP3 patch headers to all of them.
    + Drop patch against mkdist. (CVS-related, obsolete)
  * Update Makefile so that
    + all hardening flags are passed properly
    + $(DESTDIR) is honoured (no more need for override_dh_auto_install)
    + Upstream Changes file is no more installed (done by
      dh_installchangelogs automatically)
    + the package no more FTBFS with "ld --as-needed" (Closes: #641561, LP:
      #770864)
  * debian/clean: Make clean target work even when no patches are applied.
  * Extend long package description a bit.
  * Add a watch file.
  * Declare compliance with Debian Policy 3.9.8.
  * Convert debian/copyright to machine-readable DEP5 format.
  * Fix typos in 0.0.10-1 and 0.0.10-2 debian/changelog entries.

 -- Axel Beckert <abe@debian.org>  Wed, 21 Sep 2016 03:01:00 +0200

root-tail (1.2-3) unstable; urgency=low

  * Bump Standards-Version to 3.8.0 (no changes)
  * Fix build depends to be on x11proto-core-dev instead of x-dev
  * Fix copyright notice
  * Don't ignore errors in clean target

 -- Stephen Gran <sgran@debian.org>  Sun, 14 Sep 2008 19:42:42 +0100

root-tail (1.2-2) unstable; urgency=low

  * Man page typo fix (closes: #343230)
  * Fix Build-Depends to only use what's needed
  * s/int/unsigned int/ for width and height arguments to XParseGeometry
  * Update standards version to 3.6.2 (no changes)
  * Update FSF address

 -- Stephen Gran <sgran@debian.org>  Sat, 24 Dec 2005 13:23:16 +0000

root-tail (1.2-1) unstable; urgency=low

  * New Upstream Version
  * Patch to fix font initialization (closes: #298708)
    (thanks Manuel Menal <mmenal@hurdfr.org>)

 -- Stephen Gran <sgran@debian.org>  Sat, 12 Mar 2005 08:22:31 -0500

root-tail (1.1+CVS-1) unstable; urgency=low

  * CVS checkout
  * Upstream fixed initializing the window twice (made some interlacing
    problems)(closes: #243724)
  * Font fix - uses an expandable field, in case the default one or the
    specified one isn't found (hopefully will close 245852, but cannot
    reproduce here, so cannot test).

 -- Stephen Gran <sgran@debian.org>  Mon, 26 Apr 2004 21:15:17 -0400

root-tail (1.1-1) unstable; urgency=low

  * New Upstream Version

 -- Stephen Gran <sgran@debian.org>  Sun, 11 Apr 2004 13:39:14 -0400

root-tail (0.2-1) unstable; urgency=low

  * New upstream version
  * Updated to Standards Version 3.6.1

 -- Stephen Gran <sgran@debian.org>  Sun, 10 Aug 2003 15:59:20 -0400

root-tail (0.1.0-4) unstable; urgency=low

  * New Maintainer
  * Updated to latest policy

 -- Stephen Gran <steve@lobefin.net>  Sun, 20 Oct 2002 21:54:41 -0400

root-tail (0.1.0-3) unstable; urgency=low

  * Delete stray pixels on the right side (Closes: #130728)

 -- Marco d'Itri <md@linux.it>  Sat, 26 Jan 2002 14:28:18 +0100

root-tail (0.1.0-2) unstable; urgency=medium

  * Added conflict with webrt (<= 1.0.6-3).
  * Added support for named pipes.

 -- Marco d'Itri <md@linux.it>  Wed,  9 Jan 2002 19:27:22 +0100

root-tail (0.1.0-1) unstable; urgency=medium

  * New maintainer, new upstream maintainer (Closes: #119989).
  * The source code is now readable.
  * Updated to latest policy.
  * Fixed some small bugs and added some features, updated man page.
  * Text is redrawed when using SIGUSR2.
  * Added -reverse and -no-filename options.
  * Open files are reopened if renamed.
  * Open files are seeked to start if truncated (Closes: #116580).

 -- Marco d'Itri <md@linux.it>  Fri, 21 Dec 2001 19:06:28 +0100

root-tail (0.0.10-3) unstable; urgency=low

  * Fixed build-depends. (Closes: #101228)
  * Now root-tail works with log rotation, thanks Erwan. (Closes: #101444)

 -- Peter Novodvorsky <nidd@debian.org>  Sun,  1 Jul 2001 00:02:30 +0400

root-tail (0.0.10-2) unstable; urgency=low

  * Fixed default geometry as Wichert requested. Now 160x20.
        (Closes: Bug#41414)

 -- Peter Novodvorsky <nidd@debian.org>  Fri, 15 Jun 2001 02:26:18 +0400

root-tail (0.0.10-1) unstable; urgency=low

  * Changed name from rt to root-tail.
  * New version, new maintainer, cleaned up with more features
    (Closes: #23584, #26191, #28503, #31221, #41411, #41412,
             #41413, #41415, #91058, #91630, #93878)

 -- Peter Novodvorsky <nidd@debian.org>  Sat, 14 Apr 2001 01:10:48 +0400

rt (mbm0606-1.2) frozen unstable; urgency=low

  * Non-maintainer upload.
  * debian/control (Maintainer): valid address.
  * debian/menu: remove "icon=none".
  * debian/menu: we are "rt", not "Rt".
  * debian/menu: correct section to "Apps/System" as opposed to "system".
  * debian/*.ex: dh_make cruft; removed.
  * debian/dirs: removed unused usr/sbin.

 -- James Troup <james@nocrew.org>  Sun, 25 Oct 1998 05:55:14 +0000

rt (mbm0606-1.1) unstable; urgency=low

  * non-maintainer (binary-only) upload for Alpha
  * changed YAFHC i386 architecture to 'any'
  * changed -L/usr/X11/lib to -L/usr/X11R6/lib
  * current was unused at rt.c:533 because all uses were commented out

 -- Paul Slootman <paul@debian.org>  Mon,  7 Sep 1998 22:29:37 +0200

rt (mbm0606-1) unstable; urgency=low

  * Fixes 'lack of copyright' bug (bug #many of them)

 -- Kevin Poorman <ewigin@softhome.net>  Sat, 25 Jul 1998 22:53:23 -0500

rt (0.4-1) unstable; urgency=low

  * Initial Release.

 -- Kevin Poorman <ewigin@softhome.net>  Tue, 19 May 1998 22:53:23 -0500
